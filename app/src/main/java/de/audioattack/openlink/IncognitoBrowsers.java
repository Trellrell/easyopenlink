/*
 * SPDX-FileCopyrightText: 2017 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink;

/*
 * package names must be added to queries in manifest.
 */
public final class IncognitoBrowsers {

    /**
     * Firefox for Android version 79 and above does not support opening incognito tabs via Intents
     * anymore. I opened a feature request, but it was closed without bringing back the feature.
     * This means that only outdated versions of Firefox can be used to open links in incognito
     * mode.
     * <p>
     * See: https://codeberg.org/marc.nause/easyopenlink/issues/21
     */
    public static final IncognitoBrowser FIREFOX =
            new IncognitoBrowser(
                    "org.mozilla.firefox",
                    "org.mozilla.gecko.LauncherActivity",
                    2015503969,
                    2015711849, /* source: https://androidapksfree.com/firefox/org-mozilla-firefox/old/*/
                    "private_tab"
            );

    public static final IncognitoBrowser FIREFOX_LITE =
            new IncognitoBrowser(
                    "org.mozilla.rocket",
                    "org.mozilla.rocket.activity.PrivateModeActivity",
                    16697, // just a guess
                    null
            );

    public static final IncognitoBrowser FENNEC_FDROID =
            new IncognitoBrowser(
                    "org.mozilla.fennec_fdroid",
                    "org.mozilla.gecko.LauncherActivity",
                    550000,
                    689420,
                    "private_tab"
            );

    public static final IncognitoBrowser ICECAT =
            new IncognitoBrowser(
                    "org.gnu.icecat",
                    "org.mozilla.gecko.LauncherActivity",
                    550000,
                    689420,
                    "private_tab"
            );

    public static final IncognitoBrowser JELLY =
            new IncognitoBrowser(
                    "org.lineageos.jelly",
                    "org.lineageos.jelly.MainActivity",
                    1,
                    "extra_incognito"
            );

    public static final IncognitoBrowser JELLY_PLAYSTORE =
            new IncognitoBrowser(
                    "org.droidtr.jelly",
                    "org.droidtr.jelly.MainActivity",
                    1,
                    "extra_incognito"
            );

    public static final IncognitoBrowser JQUARKS =
            new IncognitoBrowser(
                    "com.oF2pks.jquarks",
                    "org.lineageos.jelly.MainActivity",
                    1,
                    "extra_incognito"
            );

    public static final IncognitoBrowser LIGHTNING =
            new IncognitoBrowser(
                    "acr.browser.lightning",
                    "acr.browser.lightning.IncognitoActivity",
                    97,
                    null
            );

    public static final IncognitoBrowser MIDORI =
            new IncognitoBrowser(
                    "org.midorinext.android",
                    "org.midorinext.android.IncognitoActivity",
                    6,
                    null
            );

    private IncognitoBrowsers() {
    }
}


