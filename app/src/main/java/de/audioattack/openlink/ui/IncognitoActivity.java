/*
 * SPDX-FileCopyrightText: 2017 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

/**
 * Handles sharing of selected text in incognito mode.
 */
public class IncognitoActivity extends Activity {

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get intent, action and MIME type
        final Intent intent = getIntent();
        final String action = intent.getAction();
        final String type = intent.getType();

        if (type != null && Intent.ACTION_SEND.equals(action)) {
            final Intent newIntent = new Intent(getApplicationContext(), MenuActivity.class);
            newIntent.setAction(action);
            newIntent.setType(type);
            final Bundle extras = intent.getExtras();
            if (extras != null) {
                extras.putString(Intent.EXTRA_TEXT, intent.getStringExtra(Intent.EXTRA_TEXT));
                extras.putBoolean(MenuActivity.EXTRA_BOOLEAN_INCOGNITO, true);
                newIntent.putExtras(extras);
            }
            startActivity(newIntent);

        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setVisible(true);
    }

}
