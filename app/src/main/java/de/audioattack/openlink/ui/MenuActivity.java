/*
 * SPDX-FileCopyrightText: 2014 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.audioattack.openlink.IntentFactory;
import de.audioattack.openlink.R;
import de.audioattack.openlink.conf.ComponentConfigurator;
import de.audioattack.openlink.conf.RequirementsChecker;
import de.audioattack.openlink.logic.Tool;

/**
 * Handles sharing of selected text.
 */
public class MenuActivity extends Activity {

    public static final String EXTRA_BOOLEAN_INCOGNITO = MainActivity.class + ".incognito";

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get intent, action and MIME type
        final Intent intent = getIntent();
        final String action = intent.getAction();
        final String type = intent.getType();

        if (type != null && Intent.ACTION_SEND.equals(action)) {
            handleSendText(intent);
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        new ComponentConfigurator(this)
                .setEnabled(
                        IncognitoActivity.class,
                        RequirementsChecker.isIncognitoBrowserInstalled(this));

        finish();
    }

    /**
     * Handles ACTION_SEND intent received via press on share item. Extracts
     * URls from input text (contained in Intent) and opens found URLs.
     *
     * @param intent contains shared text
     */
    private void handleSendText(@NonNull final Intent intent) {

        final String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {

            final Set<String> uris = Tool.getUris(sharedText);
            final List<String> errors = new ArrayList<>();

            for (final String uri : uris) {

                try {

                    if (URLUtil.isValidUrl(uri)) {

                        final Intent newIntent =
                                IntentFactory.createIntent(
                                        getApplicationContext(),
                                        Uri.parse(uri),
                                        intent.getBooleanExtra(EXTRA_BOOLEAN_INCOGNITO, false));

                        startActivity(newIntent);
                    }
                } catch (Exception ex) {
                    errors.add(uri);
                }

            }

            if (!errors.isEmpty()) {
                final StringBuilder sb = new StringBuilder();
                final String lineSeparator = System.getProperty("line.separator");

                for (final String s : errors) {
                    if (sb.length() > 0) {
                        sb.append(lineSeparator);
                    }
                    sb.append(s);
                }

                Toast.makeText(getApplicationContext(), this.getString(R.string.error_unable_to_open, sb.toString()), Toast.LENGTH_LONG)
                        .show();
            } else if (uris.isEmpty()) {
                Toast.makeText(getApplicationContext(), this.getString(R.string.error_no_url_found), Toast.LENGTH_LONG).show();
            }
        }
    }

}
