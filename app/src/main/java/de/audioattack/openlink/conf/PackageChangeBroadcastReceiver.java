/*
 * SPDX-FileCopyrightText: 2017 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink.conf;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import de.audioattack.openlink.ui.IncognitoActivity;

public class PackageChangeBroadcastReceiver extends android.content.BroadcastReceiver {

    @Override
    public void onReceive(@NonNull final Context context, @Nullable final Intent intent) {

        if (intent != null
                && (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())
                || Intent.ACTION_MY_PACKAGE_REPLACED.equals(intent.getAction()))) {

            addPackageRelatedReceivers(context, this);
        }

        new ComponentConfigurator(context).setEnabled(IncognitoActivity.class, RequirementsChecker.isIncognitoBrowserInstalled(context));
    }

    private static void addPackageRelatedReceivers(@NonNull final Context context, @Nullable final PackageChangeBroadcastReceiver broadcastReceiver) {

        final IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        filter.addAction(Intent.ACTION_MY_PACKAGE_REPLACED);

        context.getApplicationContext().registerReceiver(broadcastReceiver, filter);
    }

}
