/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink.logic;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashSet;
import java.util.Set;

public class Tool {

    /**
     * Removes "less than", "greater than", whitespaces and invisible separators (\p{Z}), invisible
     * control characters (\p{C}), and punctuation characters including brackets (\p{P}). See
     * http://www.regular-expressions.info/unicode.html for more details.
     */
    private static final String REGEX_REMOVE_FROM_URL = "[\\p{Z}\\p{P}\\p{C}<>]";

    private static final String REGEX_EXCEPTIONS = "[^/]";

    /**
     * Retrieves all Strings which look remotely like URLs from a text.
     *
     * @param sharedText text to scan for URLs.
     * @return potential URLs
     */
    public static Set<String> getUris(@Nullable final String sharedText) {

        final Set<String> result = new HashSet<>();

        if (sharedText != null) {

            final String[] array = sharedText.split("\\p{Space}");

            for (String s : array) {

                s = trim(s);

                if (!TextUtils.isEmpty(s)) {
                    if (s.matches(".+://.+")) {
                        result.add(removeHeadingGibberish(s));
                    } else if (s.matches(".+\\..+")) {
                        result.add("http://" + s);
                    }
                }
            }
        }

        return result;
    }

    private static String removeHeadingGibberish(@NonNull final String input) {

        int start = 0;

        for (int i = input.indexOf("://") - 1; i >= 0; i--) {

            if (!input.substring(i, i + 1).matches("\\p{L}")) {

                start = i + 1;
                break;
            }
        }

        return input.substring(start);
    }

    private static String trim(@Nullable final String input) {

        if (input == null || input.length() < 1) {
            return input;
        } else {

            String output = input;

            while (output.length() > 0 && output.substring(0, 1).matches(REGEX_REMOVE_FROM_URL)) {
                output = output.substring(1);
            }

            if (isValidWikipediaLink(output)) {
                output = removeSuperfluousClosingBracketFromWikipediaLink(output);
            } else {

                while (output.length() > 0
                        && output.substring(output.length() - 1).matches(REGEX_EXCEPTIONS)
                        && output.substring(output.length() - 1).matches(REGEX_REMOVE_FROM_URL)) {
                    output = output.substring(0, output.length() - 1);
                }
            }

            return output;
        }
    }

    /* Fixes Strings like "https://en.wikipedia.org/wiki/Black_Flag_(band))"
     */
    private static String removeSuperfluousClosingBracketFromWikipediaLink(@NonNull String output) {
        return output.substring(0, output.indexOf(')') + 1);
    }

    /**
     * Finds out if a string is a Mediawiki link like https://en.wikipedia.org/wiki/Black_Flag_(band)
     *
     * @param string string to check
     * @return {@code true} if it seems to be a Mediawiki link with parentheses, else {@code false}
     */
    private static boolean isValidWikipediaLink(@NonNull final String string) {

        final String[] parts = string.split("//");
        final String lastPart = parts.length > 0 ? parts[parts.length - 1] : null;

        if (lastPart == null) {
            return false;
        }

        final int indexOfOpen = lastPart.indexOf('(');
        final int indexOfClose = lastPart.lastIndexOf(')');

        return indexOfOpen >= 0 && indexOfClose > indexOfOpen;
    }
}
