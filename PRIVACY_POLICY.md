# Privacy Policy

Easy Open Link ("the app") does not collect, store or transmit any personal information. 

The app searches texts which are shared with the app by the user for URLs and starts third-party applications which are registered to handle these URLs. The app queries the Android operating system for browsers which can be used to open URLs in private or incognito mode. Apart from this the app does not communicate with any other systems or components.

This document will be updated when necessary. This Privacy Policy is applicable from version 1.5.4 onwards.

Last update: 2020-02-29